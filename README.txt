Longest Common Subsequence
Ryan Waskiewicz
April 2013

INTRODUCTION
This program finds the Longest Common Subsequence of two strings.  We
know that if s1 and s2 are strings, and c1 and c2 are characters, then
LCS(s1+c1,s2+c2) is:
     LCS(s1,s2)+c1 if c1==c2
     LCS(s1+c1,s2) if c1!=c2 and LCS(s1+c1,s2) > LCS(s1,s2+c2)
     LCS(s1,s2+c2) if c1!=c2 and LCS(s1,s2+c2) > LCS(s1+c1,s2)
Of course, LCS(s1,empty)=LCS(empty,s2)=empty.

INSTALLATION
The following files should be present:
     LCS.java		- used to find the longest common subsequence
     TestLCS.java 	- test driver for LCS.java.  Identical to the one given by Dr. Aronis


USAGE
The file TestLCS.java has some examples.  Compile and run with Java 1.6.0_41 or later:
     javac TestLCS.java
     java  TestLCS
The file TestLCS.java has two examples.  The first illustrates
correctness of the implementation.  The second demonstrates that the
runtime is actually quadratic.

PROBLEMS
For string lengths 40-120 (approximately), the time it takes to compute
a solution (in Time/Length^2) may be slightly longer than that found in
Dr. Aronis' implementation. However, after length of ~120 is computed,
the runtimes in my program will match Dr. Aronis' runtimes.

