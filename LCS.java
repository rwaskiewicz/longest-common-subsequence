// Class: LCS.java
// Author: Ryan Waskiewicz
// CS 1501 - Spring 2013 - Dr. Aronis - T/H 6pm
// Description: Given two strings, will find the largest common 
// subsequence of the two strings and return it.  To be used with
// TestLCS.java

public class LCS {
	private static String[][] memo;								//Memo of Strings for LCS
	private static char[] xString;								//Storage for String 1
	private static char[] yString;								//Storage for String 2
	private static boolean memoize;								//determines whether to check or not
	
	public static String findLCS(String s1, String s2) {
		memoize = true;											//need memo for O(n^2) runtime
		xString = s1.toCharArray();
		yString = s2.toCharArray();
		memo = new String[s1.length()][s2.length()];
		return calculateLCS(0,0);
	}
	
	//calculate the LCS
	public static String calculateLCS(int xSize, int ySize) {
		//prevent out of bounds errors
		if (xSize == xString.length || ySize == yString.length) {
			return "";
		}	
		//base case - check memo to see if this subproblem has already been solved
		else if (memoize && memo[xSize][ySize] != null) {
			return memo[xSize][ySize];
		}
		//if the end characters of the strings (or sub strings) match
		//compute answer to subproblem and prepend the character since
		//a match was found
		else if (xString[xSize] == yString[ySize]) {
			String addedLetter = String.valueOf(xString[xSize]);
			memo[xSize][ySize] = calculateLCS(xSize+1, ySize+1);
			//this string op is costly 
			memo[xSize][ySize] = addedLetter.concat(memo[xSize][ySize]);
		}
		//the end characters of the strings (or sub strings) don't match
		//find answers to respective subproblems, take the longest of the two
		else {
			String posString1 = calculateLCS(xSize+1, ySize);
			String posString2 = calculateLCS(xSize, ySize+1);
			if (posString1.length() > posString2.length()) {
				memo[xSize][ySize] = posString1;
			}
			else {
				memo[xSize][ySize] = posString2;
			}
		}
		return memo[xSize][ySize];
	}	
}